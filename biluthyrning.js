const url = "http://localhost:8080/api";
const apiVersion = "v1"

// Inloggning
let getUsername = function() {
    return getCookie("username");
}
let getPassword = function() {
    return getCookie("password");
}

let getLoggedInUser = function() {
    // Todo
    return {
        "id": 1,
        "name": "Pippa Mediaslargas",
        "address": "Villerkulla 5"
    }
}

$(function() {
    navigateTo("home");
    getAuthorized("customers").done(() => {
        $(".logged-in").css("visibility", "visible");
        $("a[data-bs-target='#loginModal'").text("Logga ut");
        $("a[data-bs-target='#loginModal'").attr("data-action", "logout")
        $("a[data-bs-target='#loginModal'").attr("data-bs-target", "")
    }).fail(() => {
        console.log("fail");
        $(".logged-in").css("visibility", "hidden");
        $("a[data-bs-target='#loginModal'").text("Logga in");
        $("a[data-bs-target='#loginModal'").attr("data-action", "login")
    })
})

$(document).on("click", "a[data-action='logout']", () => {
    setCookie("username", "");
    setCookie("password", "");
    window.location.reload(true);
});

$("#loginModal").on("hidden.bs.modal", (ev) => {
    $("#usernameInput").val("")
    $("#passwordInput").val("");
});

$("#loginSubmitBtn").on("click", () => {
    setCookie("username", $("#usernameInput").val());
    setCookie("password", $("#passwordInput").val());
    window.location.reload(true);
});

// Ajax

const getAuthorized = function(apiPath) {
    return $.ajax({
        url: url + "/" + apiVersion + "/" + apiPath,
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Basic " + btoa(getUsername() + ":" + getPassword()));
        }
    });
}

const postAuthorized = function(apiPath, formData, type = "POST") {
    return $.ajax({
        type: type,
        url: url + "/" + apiVersion + "/" + apiPath,
        data: JSON.stringify(formData),
        headers: {
            'Content-Type':'application/json'
        },
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Basic " + btoa(getUsername() + ":" + getPassword()));
        }
      }).done(function (data) {
        console.log(data);
      });
}

const deleteAuthorized = function(apiPath, formData) {
    return $.ajax({
        type: "delete",
        url: url + "/" + apiVersion + "/" + apiPath,
        data: JSON.stringify(formData),
        headers: {
            'Content-Type':'application/json'
        },
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Basic " + btoa(getUsername() + ":" + getPassword()));
        }
      }).done(function (data) {
        console.log(data);
      });
}

// Tabell relaterat

const populateTable = function(table, data) {
    let tableRows = ""
    let tableColumns = [];

    $(table + " thead th").each(function() {
        tableColumns.push(this.dataset.value);
    });

    data.forEach(object => {
        tableRows += createTableRow(object, tableColumns);
    });

    $(table + " tbody").html(tableRows);

    if ($(table).hasClass("sortable")) {
        makeSortable(table);
    }
}

const createTableRow = function(object, tableColumns) {
    let tableRow = "";
    tableRow += "<tr " + "data-object-id='" + object.id + "' onclick='select(this)'>";
        tableColumns.forEach(function(objectPropertyPath) {
            tableRow += "<td>" + traverseObject(object, objectPropertyPath) + "</td>";
        })
    tableRow += "</tr>";
    return tableRow;
}

const traverseObject = function(object, path) {
    pathArray = path.split(".");
    let value = recursiveTraversal(object, pathArray);
    if (value === undefined) {
        value = "";
    }
    return value;
}

const recursiveTraversal = function(object, pathArray)  {
    if (pathArray.length === 0) {
        return object;
    } else {
        return recursiveTraversal(object[pathArray.shift()], pathArray);
    }
}

const sortBy = {
    "number" : function(a, b) {
        return a - b;
    },
    "text" : function(a, b) {
        if (a < b) {
            return -1;
        }
        if (a > b) {
            return 1;
        }
        return 0;
    }
}

const makeSortable = function(table) {
    $(table + " thead th").each(function() {
        $(this).on("click", function() {
            let columnHeader = this;
            let columnNr = $(table + " thead th").index(columnHeader);
            let type = columnHeader.dataset.type;
            let order = columnHeader.dataset.order;

            $(table + " tbody").html(function() {
                if (order == undefined) {
                    $(table + " thead th").removeAttr("data-order");
                    columnHeader.dataset.order = "ASC";
   
                    return $(table + " tbody tr").sort((a, b) => {
                        let aRow = a.children[columnNr].innerText.toLowerCase();
                        let bRow = b.children[columnNr].innerText.toLowerCase();
                        return sortBy[type](aRow, bRow)
                    });
                }

                columnHeader.dataset.order = (order === "ASC") ? "DESC" : "ASC";
                return $(table + " tbody tr").get().reverse();
            });
        });
    });
}

const select = function(element) {
    $(element).parent().children().removeAttr("data-selected");
    element.dataset.selected = true;
}

const getSelectedId = function(table) {
    return $(table + " tr[data-selected='true']").attr("data-object-id");
}


// Cookie hantering

function setCookie(cname, cvalue) {
    document.cookie = cname + "=" + cvalue + "; secure";
}

// Cookie funktion taget från W3Schools
function getCookie(cname) {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
        c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
        }
    }
    return "";
}

const datesAreValid = function(startDate, stopDate) {
    let yesterday = (date = new Date()) => {date.setDate(date.getDate() - 1); return date}

    let startLaterThanYesterday = new Date(startDate) > yesterday() && startDate != "";
    let stopLaterThanStart = new Date(startDate) < new Date(stopDate);

    if (!startLaterThanYesterday) {
        $("#startDateInput").addClass("is-invalid");
        $("#startDateInput").removeClass("is-valid");
    } else {
        $("#startDateInput").removeClass("is-invalid");
        $("#startDateInput").addClass("is-valid");
    }
    if (!stopLaterThanStart) {
        $("#stopDateInput").addClass("is-invalid");
        $("#stopDateInput").removeClass("is-valid");
    } else {
        $("#stopDateInput").removeClass("is-invalid");
        $("#stopDateInput").addClass("is-valid");
    }
    return startLaterThanYesterday && stopLaterThanStart;
}
