let loadedCustomers = [];

const initCustomers = function() {
    getAuthorized("customers").done(function(customers) {
        loadedCustomers = customers;
        populateTable("#customerTable", loadedCustomers);

        getAuthorized("myorders").done(function(orders) {
            let indexOfColumn = $("#customerTable thead th[data-value='nrOfOrders'").index();

            
            $("#customerTable tbody tr").each((id, tr) => {
                let thisCustomersOrders = orders.filter((o) => o.customer.id == tr.dataset.objectId);
                tr.cells[indexOfColumn].innerHTML = thisCustomersOrders.length;
            })
        });
    });
}

$(document).on("change", "#lessOrdersThanCheckBox, #lessOrdersThan", () => {
    if ($("#lessOrdersThanCheckBox").prop("checked")) {
        let indexOfColumn = $("#customerTable thead th[data-value='nrOfOrders'").index();
        $("#customerTable tbody tr").each((id, tr) => {
            if(parseInt($("#lessOrdersThan").val()) <= parseInt(tr.cells[indexOfColumn].innerHTML)) {
                $(tr).addClass("lessThanRow");
            } else {
                $(tr).removeClass("lessThanRow");
            }
        })
    } else {
        $("#customerTable tbody tr").each((id, tr) => {
            $(tr).removeClass("lessThanRow");
        })
    }
});

$(document).on("change", "#moreOrdersThanCheckBox, #moreOrdersThan", () => {
    if ($("#moreOrdersThanCheckBox").prop("checked")) {
        let indexOfColumn = $("#customerTable thead th[data-value='nrOfOrders'").index();
        $("#customerTable tbody tr").each((id, tr) => {
            if(parseInt($("#moreOrdersThan").val()) >= parseInt(tr.cells[indexOfColumn].innerHTML)) {
                $(tr).addClass("moreThanRow");
            } else {
                $(tr).removeClass("moreThanRow");
            }
        })
    } else {
        $("#customerTable tbody tr").each((id, tr) => {
            $(tr).removeClass("moreThanRow");
        })
    }
});

