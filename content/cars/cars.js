let loadedCars = {};

const initCars = function() {
    getAuthorized("cars").done(function(cars) {
        loadedCars = cars;
        populateTable("#carTable", loadedCars);
    });
}



$(document).on("click", "#deleteCarOpenModalBtn", function() {
    $("#CarModal .modal-dialog").load("content/cars/deleteCarModalContent.html", "", () => {
        let selectedCar = loadedCars.find((car) => {
            return car.id == getSelectedId("#carTable");
        })
    
        let formSelected = $("#CarModal div[data-objectid]");
    
        formSelected[0].dataset.objectid = selectedCar.id;
    
        formSelected.html("Bil: <span style='font-weight: bold'>" + selectedCar.name + "</span> <br>"
        + "Storlek: <span style='font-weight: bold'>" + selectedCar.model + "</span><br>"
        + "Pris per dag: <span style='font-weight: bold'>" + selectedCar.pricePerDay + "</span>");
    }); 
});

$(document).on("click", "#deleteCarSubmitBtn", function() {
    const carToDelete = {
        id : getSelectedId("#carTable")
    }

    getAuthorized("myorders").done(orders => {
        let ordersWithCar = orders.filter(order => order.car.id == carToDelete.id);
        console.log(ordersWithCar)
        if (ordersWithCar[0] != undefined) {
            console.log(ordersWithCar)

            let replaceCar;

            getAuthorized("cars").done(cars => {

                cars = cars.filter(car => car.id != carToDelete.id);

                let potentialCars = cars.filter(car => {
                    return car.model == ordersWithCar[0].car.model;
                });

                if (potentialCars > 0) {
                    replaceCar = potentialCars[0];
                } else if (cars.length > 0) {
                    replaceCar = cars[0];
                }

                let carsChanged = 0;
                ordersWithCar.forEach(order => {
                    order.car = replaceCar;
                    postAuthorized("updateorder", order, "PUT").done((data) => {
                        carsChanged++;
                        if (carsChanged === ordersWithCar.length) {
                            deleteCar(carToDelete);
                        }
                    });
                });
                
            });
            
        } else {
            deleteCar(carToDelete);
        }
    });

    

    
});

const deleteCar = function(carToDelete) {
    deleteAuthorized("deletecar", carToDelete).done(() => {
        $("#carTable tr[data-object-id=" + carToDelete.id + "]").remove();
        $("#deleteCarSubmitBtn").prop('disabled', true);
        $("#deleteCarModalLabel").text("Bil borttagen!");
    })
}

$(document).on("click", "#updateCarOpenModalBtn", function() {
    $("#CarModal .modal-dialog").load("content/cars/updateCarModalContent.html", "", () => {
        let selectedCar = loadedCars.find((car) => car.id == getSelectedId("#carTable"));
        if (selectedCar != undefined) {
            $("#carNameInput").val(selectedCar.name),
            $("#carModelInput").val(selectedCar.model),
            $("#carPricePerDayInput").val(selectedCar.pricePerDay)
        } else {
            $("#updateCarModalLabel").text("Måste välja en bil!");
            $("#updateCarModalLabel").parent().addClass("warning-modal")
            $("#carNameInput").prop("disabled", true);
            $("#carModelInput").prop("disabled", true);
            $("#carPricePerDayInput").prop("disabled", true);
            $("#updateCarSubmitBtn").prop("disabled", true);
        }
        
        
    });
});

$(document).on("click", "#updateCarSubmitBtn", function() {
    let updatedCar = {
        id : getSelectedId("#carTable"),
        name : $("#carNameInput").val(),
        model : $("#carModelInput").val(),
        pricePerDay : $("#carPricePerDayInput").val()
    }

    postAuthorized("updatecar", updatedCar, "PUT").done((data) => {
        getAuthorized("cars").done(function(cars) {
            loadedCars = cars;
            selectedCar = loadedCars.find((car) => car.id == getSelectedId("#carTable"));
            let newSelectedRow = "";
            $("#carTable thead th").each((id, th) => {
                newSelectedRow += "<td>" + traverseObject(updatedCar, $(th).attr("data-value")) + "</td>";
            });
            $("#carTable tr[data-object-id=" + getSelectedId("#carTable") + "]").html(newSelectedRow);

            $("#updateCarSubmitBtn").prop('disabled', true);
            $("#updateCarModalLabel").parent().addClass("successful-modal")
            $("#updateCarModalLabel").text("Bil uppdaterad!");
        });
        
    });
});

$(document).on("click", "#createCarOpenModalBtn", function() {
    $("#CarModal .modal-dialog").load("content/cars/createCarModalContent.html", "", () => {

    }); 
});

$(document).on("click", "#createCarSubmitBtn", function() {
    let newCar = {
        name : $("#carNameInput").val(),
        model : $("#carModelInput").val(),
        pricePerDay : $("#carPricePerDayInput").val()
    }

    console.log(newCar);
    postAuthorized("addcar", newCar).done((data) => {
        let rx = "id=([\\d]+)";
        newCar.id = data.match(rx)[1];
        $("#carTable tbody").append(createTableRow(newCar, ["name", "model", "pricePerDay"]));
        
        $("#createCarSubmitBtn").prop('disabled', true);
        $("#createCarModalLabel").parent().addClass("successful-modal")
        $("#createCarModalLabel").text("Bil Skapad!");
    });
});



