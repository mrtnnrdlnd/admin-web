let loadedOrders = [];

const initOrders = function() {
    getAuthorized("myorders").done(function(orders) {
        loadedOrders = orders;
        populateTable("#ordertable", loadedOrders);
    });
}

$(document).on("click", "#updateOrderModalBtn", function() {
    $("#updateOrderModal .modal-dialog").load("content/orders/updateOrderModalContent.html", "", () => {
        let selectedOrder = loadedOrders.find((order) => {
            return order.id == getSelectedId("#ordertable");
        })

        let options = [];

        getAuthorized("cars").done(function(cars) {
            loadedCars = cars;
            cars.forEach(car => {
                options.push('<option value=' + car.id + '>' + car.name + '</option>');
            });

            $("#carSelect").empty().append('<option value=' + selectedOrder.car.id + '>' + selectedOrder.car.name + '</option>' + options.join(""));

        });

        $("#startDateInput")[0].value = new Date(selectedOrder.startDate).toISOString().split('T')[0].slice(0, 10);
        $("#stopDateInput")[0].value = new Date(selectedOrder.stopDate).toISOString().split('T')[0].slice(0, 10);

    });
});

$(document).on("click", "#updateOrderSubmitBtn", function() {
    let updatedOrder = {
        id : getSelectedId("#ordertable"),
        car : loadedCars.find((car) => {
            return car.id == $("#carSelect").val();
        }),
        customer : getLoggedInUser(),
        startDate : $("#startDateInput").val(),
        stopDate : $("#stopDateInput").val(),
    }

    postAuthorized("updateorder", updatedOrder, "PUT").done((data) => {
        getAuthorized("myorders").done(function(orders) {
            loadedOrders = orders;
            let newSelectedRow = "";
            updatedOrder = loadedOrders.find(order => {
                return order.id == getSelectedId("#ordertable");
            });
            console.log(updatedOrder)
            $("#ordertable thead th").each((id, th) => {
                console.log(traverseObject(updatedOrder, $(th).attr("data-value")))
                newSelectedRow += "<td>" + traverseObject(updatedOrder, $(th).attr("data-value")) + "</td>";
            });
            $("#ordertable tr[data-object-id=" + getSelectedId("#ordertable") + "]").html(newSelectedRow);
        });
        $("#updateOrderModalLabel").parent().addClass("successful-modal");
        $("#updateOrderModalLabel").html("Uppdateringen är sparad!");
        $("#carSelect").prop('disabled', true)
        $("#startDateInput").prop('disabled', true)
        $("#stopDateInput").prop('disabled', true)
        $("#updateOrderSubmitBtn").prop('disabled', true)
    });
    
  
});